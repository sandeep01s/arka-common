package com.blorecomp.teach.servercommon.appModel;

public class AppRegisterRequest {


    String name;
    String dept;
    String schoolId;
    String email;
    String deviceId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override public String toString() {
        return "AppRegisterRequest{" +
          "name='" + name + '\'' +
          ", dept='" + dept + '\'' +
          ", schoolId='" + schoolId + '\'' +
          ", email='" + email + '\'' +
          ", deviceId='" + deviceId + '\'' +
          '}';
    }
}
