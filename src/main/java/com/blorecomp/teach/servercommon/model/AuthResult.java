package com.blorecomp.teach.servercommon.model;

public class AuthResult {

    private String name;
    private boolean status;
    private String userId;
    private String clientID;
    private String sessionId;

    public AuthResult() {
    }

    public AuthResult(String name, boolean status, String userId, String clientID, String sessionId) {
        this.name = name;
        this.status = status;
        this.userId = userId;
        this.clientID = clientID;
        this.sessionId = sessionId;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
