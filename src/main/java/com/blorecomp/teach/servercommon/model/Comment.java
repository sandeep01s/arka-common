package com.blorecomp.teach.servercommon.model;

public class Comment {

    String contentID;
    String contentLevel;
    String comment;
    String commenterEmail;

    public Comment() {
    }

    public Comment(String contentID, String contentLevel, String comment, String commenterEmail) {
        this.contentID = contentID;
        this.contentLevel = contentLevel;
        this.comment = comment;
        this.commenterEmail = commenterEmail;
    }

    public String getContentID() {
        return contentID;
    }

    public void setContentID(String contentID) {
        this.contentID = contentID;
    }

    public String getContentLevel() {
        return contentLevel;
    }

    public void setContentLevel(String contentLevel) {
        this.contentLevel = contentLevel;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommenterEmail() {
        return commenterEmail;
    }

    public void setCommenterEmail(String commenterEmail) {
        this.commenterEmail = commenterEmail;
    }
}
