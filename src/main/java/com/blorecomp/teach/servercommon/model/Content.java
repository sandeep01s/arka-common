package com.blorecomp.teach.servercommon.model;

import java.util.Date;

public class Content extends BaseContent {

    private String[] answer;
    private String[] rightAnswer;
    private boolean userAnswer;

    public Content() {
    }

    public Content(String htmlTypeId, String associatedImgs, String question, String[] answer, String[] rightAnswer, int weight, Date crTimestamp, String crUserEmail,
        String contentTag, String topicTag, String aprEmail, String aprStatus, String comments, boolean userAnswer) {
        super(htmlTypeId, associatedImgs, question, weight, crTimestamp, crUserEmail, contentTag, topicTag,  aprEmail,  aprStatus,  comments);
        this.answer = answer;
        this.rightAnswer = rightAnswer;
        this.userAnswer = userAnswer;
    }

    public String[] getAnswer() {
        return answer;
    }

    public void setAnswer(String[] answer) {
        this.answer = answer;
    }

    public String[] getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String[] rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public boolean isUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(boolean userAnswer) {
        this.userAnswer = userAnswer;
    }

    @Override public String toString() {
        return "Content{" +
            "id='" + this.getId() + '\'' +
            ", htmlTypeId='" + this.getHtmlTypeId() + '\'' +
            ", associatedImgs='" + this.getAssociatedImgs() + '\'' +
            ", question='" + this.getQuestion() + '\'' +
            ", answer='" + answer + '\'' +
            ", rightAnswer='" + rightAnswer + '\'' +
            ", weight=" + this.getWeight() +
            ", crTimestamp=" + this.getCrTimestamp() +
            ", crUserEmail='" + this.getCrUserEmail() + '\'' +
            ", contentTag='" + this.getContentTag() + '\'' +
            ", topicTag='" + this.getTopicTag() + '\'' +
            ", aprEmail='" + this.getAprEmail() + '\'' +
            ", aprStatus='" + this.getAprStatus() + '\'' +
            ", comments='" + this.getComments() + '\'' +
            '}';
    }
}
