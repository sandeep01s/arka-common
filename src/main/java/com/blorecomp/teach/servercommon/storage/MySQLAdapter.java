package com.blorecomp.teach.servercommon.storage;

import com.blorecomp.teach.servercommon.Util.CryptoUtil;
import com.blorecomp.teach.servercommon.Util.GeneralUtils;
import com.blorecomp.teach.servercommon.Util.PatternUtil;
import com.blorecomp.teach.servercommon.appModel.*;
import com.blorecomp.teach.servercommon.model.*;
import com.blorecomp.teach.servercommon.pojo.UserPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;


public class MySQLAdapter {

    private String hostName;
    private int port;
    private String DBName;
    private String username;
    private String pwd;
    private Connection con;
    private String secretPwdKey;
    private CryptoUtil cryptoUtil;

    private static final String NEW_USER = "New User";

    Logger logger = LogManager.getLogger(MySQLAdapter.class);
    private static String selectStr = "select id, associatedImgs, question, answer, rightAnswer, weight, cr_timestamp , content_tag, topic_tag, cr_user_email,comments, apr_email, apr_state from ";
    private static int quesCount = 4;
    // Dev Env
    //    public MySQLAdapter() {
    //        this.hostName = "localhost";
    //        this.port = 3306;
    //        this.username = "ssrivatsa";
    //        this.pwd = "ssrivatsa";
    //        this.DBName = "testdb3";
    //        secretPwdKey = "hhlyeullpfd123-60";
    //        cryptoUtil = new CryptoUtil();
    //    }

    // Prod Env

    public MySQLAdapter() {
        this.hostName = "localhost";
        this.port = 3306;
        this.username = "produser";
        this.pwd = "isotope@22";
        this.DBName = "arkaprod";
        secretPwdKey = "hhlyeullpfd123-60";
        cryptoUtil = new CryptoUtil();
    }

    public MySQLAdapter(String hostName, int port, String username, String pwd, String DBName) {
        this.hostName = hostName;
        this.port = port;
        this.username = username;
        this.pwd = pwd;
        this.DBName = DBName;
        this.secretPwdKey = "hhlyeullpfd123-60";
        this.cryptoUtil = new CryptoUtil();
    }

    public void init() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Connecting with : jdbc:mysql://" + this.hostName + ":" + this.port + "/" + this.DBName + " Username: " + this.username + " & Password/useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=PDT");
            con = DriverManager.getConnection(
              "jdbc:mysql://" + this.hostName + ":" + this.port + "/" + this.DBName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&autoReconnect=true", this.username, this.pwd);
            logger.info("Initialized MySQL connection to the server: Host:"+hostName+", Port:"+port+", DB: "+DBName+", User:"+username);
            this.keepConAlive();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Failed",e);
        }
    }


    public RegisterUserResult registerNewUser(UserPojo userPojo) {
        PreparedStatement stmt;
        String currentUserIdStr = "0";
        RegisterUserResult registerUserResult = new RegisterUserResult();
        ResultSet rs;
        logger.debug("Registering new Admin user: "+userPojo);
        try {
            // Check and allow user to register
            String checkUserExists = "SELECT * FROM `user` where `email`=?";
            stmt = con.prepareStatement(checkUserExists);
            stmt.setString(1, userPojo.getEmail());

            ResultSet userCheckRs = stmt.executeQuery();
            if (userCheckRs.next()) {
                registerUserResult.setAlreadyRegistered(true);
                registerUserResult.setRegistrationStatus(false);
                logger.info("User:"+username+" already exists2");
                return registerUserResult;
            }


            // Insert the new registered user row
            String insertNewRegisteredCust = "INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES (?, ?, ?, ?);";

            stmt = con.prepareStatement(insertNewRegisteredCust);
            stmt.setString(1, UUID.randomUUID().toString());
            stmt.setString(2, userPojo.getName());
            stmt.setString(3, userPojo.getEmail());
            // Decode the password, ecnrypt and store it.
            // Next time when it comes, encrypt again and check.
            stmt.setString(4, cryptoUtil.encrypt(secretPwdKey, userPojo.getPassword()));

            System.out.println(insertNewRegisteredCust);
            stmt.executeUpdate();

            stmt.close();
            registerUserResult.setRegistrationStatus(true);
            registerUserResult.setAlreadyRegistered(false);

            logger.debug("Successfully registered user: "+userPojo);
        } catch (Exception e) {
            e.printStackTrace();
            registerUserResult.setRegistrationStatus(false);
            registerUserResult.setAlreadyRegistered(false);
            logger.error("Error while registering new Admin user: "+userPojo);
            logger.error("Failed",e);
        }
        return registerUserResult;
    }

    public RegisterUserResult registerNewAppUser(AppRegisterRequest appRegisterRequest) {
        PreparedStatement stmt;
        RegisterUserResult registerUserResult = new RegisterUserResult();

        logger.debug("Registering new App user: "+appRegisterRequest);
        try {
            /*
            // TODO: Uncomment after App approval
            // Check if School ID exists
            if(!this.checkSchoolExists(appRegisterRequest.getSchoolId())) {
                throw new Exception("School ID:" + appRegisterRequest.getSchoolId() +" does not exist");
            }
            */

            // Check and allow user to register
            String checkUserExists = "SELECT * FROM `appuser` where `email`=? and schoolid=?";
            stmt = con.prepareStatement(checkUserExists);
            stmt.setString(1, appRegisterRequest.getEmail());
            stmt.setString(2,appRegisterRequest.getDeviceId());

            ResultSet userCheckRs = stmt.executeQuery();
            if (userCheckRs.next()) {
                registerUserResult.setAlreadyRegistered(true);
                registerUserResult.setRegistrationStatus(false);
                logger.info("User:"+username+" already exists2");
                return registerUserResult;
            }

            // Insert the new registered user row
            String insertNewRegisteredCust = "INSERT INTO `appuser` (`id`, `name`, `email`, `dept`, `schoolid`,`deviceid`) VALUES (?, ?, ?, ?, ?, ?);";

            stmt = con.prepareStatement(insertNewRegisteredCust);
            stmt.setString(1, UUID.randomUUID().toString());
            stmt.setString(2, appRegisterRequest.getName());
            stmt.setString(3, appRegisterRequest.getEmail());
            stmt.setString(4,appRegisterRequest.getDept());
            stmt.setString(5,appRegisterRequest.getSchoolId());
            stmt.setString(6,appRegisterRequest.getDeviceId());
            stmt.executeUpdate();

            stmt.close();
            registerUserResult.setRegistrationStatus(true);
            registerUserResult.setAlreadyRegistered(false);

            logger.debug("Successfully registered user: "+appRegisterRequest);
        } catch (Exception e) {
            e.printStackTrace();
            registerUserResult.setRegistrationStatus(false);
            registerUserResult.setAlreadyRegistered(false);
            logger.error("Error while registering new App user: "+appRegisterRequest);
            logger.error("Failed",e);
        }
        return registerUserResult;
    }

    public void updateAppUserWithSession(String email, String sessionId) {
        PreparedStatement stmt;
        logger.debug("Updating AppUser with Session ID, user, sessionID "+email+","+sessionId);
        try {
            String insertQuery = "UPDATE appuser set sessionid = ? where email = ?";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, sessionId);
            stmt.setString(2, email);
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating new UUID");
            logger.error("Failed",e);
        }
        logger.debug("Success: Updating AppUser with Session ID, user, sessionID "+email+","+sessionId);
    }

    public UUID getNewUUID(String email) {
        UUID uuid = UUID.randomUUID();
        PreparedStatement stmt;
        try {
            String insertQuery = "INSERT INTO `live_tokens` (`token`, `email`) VALUES (?, ?)";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, uuid.toString());
            stmt.setString(2, email);
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating new UUID");
            logger.error("Failed",e);
        }
        return uuid;
    }


    public Map<Integer, String> getHTMLTypes() {
        PreparedStatement stmt;
        Map<Integer, String> htmlTypes = new HashMap<>();
        logger.debug("Fetching HTML Types");
        try {
            String insertQuery = "select id, name from html_types";
            stmt = con.prepareStatement(insertQuery);
            ResultSet htmlTypesRes = stmt.executeQuery();
            while (htmlTypesRes.next()) {
                htmlTypes.put(htmlTypesRes.getInt(1), htmlTypesRes.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while fetching html tags");
            logger.error("Failed",e);
        }
        return htmlTypes;
    }

    public LinkedHashMap<String, String> getContentTags() {
        PreparedStatement   stmt;
        LinkedHashMap<String, String> contentTagsMap = new LinkedHashMap<>();
        try {
            String insertQuery = "select name, content_type from content_tags order by sl_no";
            stmt = con.prepareStatement(insertQuery);
            ResultSet contentTypes = stmt.executeQuery();
            while (contentTypes.next()) {
                contentTagsMap.put(contentTypes.getString(1), contentTypes.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while fetching contentTypes");
            logger.error("Failed",e);
        }
        return contentTagsMap;
    }

    public String getContentTypeBasedOnContentTag(String contentTag) {
        PreparedStatement stmt;
        String contentType = "content_A";
        try {
            String insertQuery = "select content_type from content_tags where name = ?";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1,contentTag);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                contentType = rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }
        return contentType;
    }

    public Map<String, String> getAllTopics() {
        PreparedStatement stmt;
        Map<String, String> mapOfTopics = new HashMap<>();
        try {
            String insertQuery = "select `name`,`desc` from topic";
            stmt = con.prepareStatement(insertQuery);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mapOfTopics.put(rs.getString(1).trim(), rs.getString(2));
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }
        return mapOfTopics;
    }

    public boolean updateContent(NewContent content) {
        PreparedStatement stmt;
        String contentCategory = getContentTypeBasedOnContentTag(content.getContentTag());
        try {
            String updateQuery = "update "+contentCategory +" SET "
              + " associatedImgs = ?"
              + ", question = ?"
              + ", answer = ?"
              + ", rightAnswer = ?"
              + ", weight = ?"
              + " where id = ?";
            stmt = con.prepareStatement(updateQuery);
            stmt.setString(1, content.getAssociatedImgs());
            stmt.setString(2, content.getQuestion());
            stmt.setString(3, content.getAnswer());
            stmt.setString(4, content.getRightAnswer());
            stmt.setInt(5,content.getWeight());
            stmt.setString(6,content.getId());
            stmt.executeUpdate();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
            return false;
        }
        return true;
    }

    public void updateContentComment(Comment comment) {
        PreparedStatement stmt;
        String contentCategory = getContentTypeBasedOnContentTag(comment.getContentLevel());
        try {

            String selecCommentQuery = "update "+contentCategory +" SET comments= CONCAT_WS('\n',comments,?) where id=?";
            stmt = con.prepareStatement(selecCommentQuery);

            StringBuilder commentStr = new StringBuilder();
            commentStr.append(GeneralUtils.getCurrentTimestamp());
            commentStr.append(":");
            commentStr.append(comment.getCommenterEmail());
            commentStr.append(": ");
            commentStr.append(comment.getComment());

            stmt.setString(1,commentStr.toString());
            stmt.setString(2,comment.getContentID());
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }
    }

    public void approveContent(ApproveContent approveContent) {
        PreparedStatement stmt;
        String contentCategory = getContentTypeBasedOnContentTag(approveContent.getContentLevel());
        try {

            String selectCommentQuery = "update "+contentCategory +" SET apr_state= 'A' , apr_email = ? where id=?";
            stmt = con.prepareStatement(selectCommentQuery);

            stmt.setString(1,approveContent.getAprEmail());
            stmt.setString(2,approveContent.getContentId());
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }
    }

    public void insertContent(NewContent content) {
        content.createContentID();
        String contentCategory = getContentTypeBasedOnContentTag(content.getContentTag());

        PreparedStatement stmt;
        try {
            String insertQuery = "INSERT INTO `"+contentCategory+ "` (`id`, `html_types_id`, `associatedImgs`, " +
              "`question`, `answer`, `rightAnswer`, `weight`, `cr_user_email`, `content_tag`, `topic_tag`) " +
              "VALUES (?,?,?,?,?,?,?,?,?,?)";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, content.getId());

            String qHTMLID = content.getHtmlTypeId();
            if(qHTMLID==null || qHTMLID.isEmpty())
                qHTMLID = "1";
            stmt.setString(2, qHTMLID);

            String qImgs = content.getAssociatedImgs();
            if(qImgs == null || qImgs.isEmpty())
                qImgs = "";
            stmt.setString(3, qImgs);
            stmt.setString(4, content.getQuestion());

            /*
            // Create map of Answers
            StringBuilder answers = new StringBuilder();
            String finalAnswer = null;
            Map<Integer, String> answersMap = content.getAnswer();

            if(!answersMap.isEmpty()) {
                for (Integer answerKey:answersMap.keySet()) {
                    answers.append(answerKey);
                    answers.append(":");
                    answers.append(answersMap.get(answerKey));
                    answers.append("|");
                }
                finalAnswer = answers.substring(0,answers.length()-1);
            }
            stmt.setString(5, finalAnswer);
            */

            String qAns = content.getAnswer();
            if(qAns == null || qAns.isEmpty())
                qAns = "";

            stmt.setString(5, qAns);

            String rightAns = content.getRightAnswer();
            if(rightAns==null || rightAns.isEmpty())
                rightAns = "";

            stmt.setString(6, rightAns);

            stmt.setInt(7,content.getWeight());

            stmt.setString(8,content.getCrUserEmail());

            String qContentTag = content.getContentTag();
            if(qContentTag == null || qContentTag.isEmpty())
                qContentTag = "Level-1";
            stmt.setString(9, qContentTag);

            String qTopicTag = content.getTopicTag();
            if(qTopicTag == null || qTopicTag.isEmpty())
                qTopicTag = "Case Study";
            stmt.setString(10,qTopicTag);

            stmt.executeUpdate();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while inserting new Content");
            logger.error("Failed",e);
        }
    }

    public boolean deleteContent(ApproveContent approveContent) {
        PreparedStatement stmt;
        boolean status = false;
        int updatedRows;
        String contentCategory = getContentTypeBasedOnContentTag(approveContent.getContentLevel());
        try {

            String selectCommentQuery = "DELETE FROM "+contentCategory +" WHERE `cr_user_email` = ? AND `id`=?";
            stmt = con.prepareStatement(selectCommentQuery);

            stmt.setString(1,approveContent.getAprEmail());
            stmt.setString(2,approveContent.getContentId());
            updatedRows = stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
            return status;
        }
        if (updatedRows == 1)
            status = true;
        else
            status = false;
        return status;
    }


    public List<Content> getContentByContentType(String email, String contentType) {

        PreparedStatement stmt;
        List<Content>     listOfContents = new ArrayList<>();
        try {
            String insertQuery = selectStr + contentType+" where cr_user_email = ?";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, email);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listOfContents.add(fillUpContent(rs));
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while getting content for user: "+email);
            logger.error("Failed",e);
        }
        return listOfContents;
    }

    public List<Content> getContentByContentTags(String email, String contentTag) {

        PreparedStatement stmt;
        List<Content>     listOfContents = new ArrayList<>();
        try {
            String contentCategory = getContentTypeBasedOnContentTag(contentTag);
            String insertQuery = selectStr + contentCategory+" where cr_user_email = ? AND" +
              " content_tag = ? ";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, email);
            stmt.setString(2,contentTag);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listOfContents.add(fillUpContent(rs));
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while getting content for user: "+email);
            logger.error("Failed",e);
        }
        return listOfContents;
    }

    public List<Content> getAllContentByContentType(String contentTag, int limit, boolean random, boolean onlyApproved) {

        PreparedStatement stmt;
        List<Content>     listOfContents = new ArrayList<>();
        String whereClause = " where apr_state='A' ";
        try {
            String contentCategory = getContentTypeBasedOnContentTag(contentTag);

            String orderBy = random ? "" : " order by RAND() ";
            String limitBy = limit == 0 ? "" : " LIMIT "+String.valueOf(limit);

            String insertQuery = selectStr + contentCategory + (onlyApproved?whereClause:"") + orderBy + limitBy;

            stmt = con.prepareStatement(insertQuery);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listOfContents.add(fillUpContent(rs));
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while getting all content for type : " + contentTag);
            logger.error("Failed",e);
        }
        return listOfContents;
    }

    private Content fillUpContent(ResultSet rs) throws SQLException {

        Content content = new Content();
        content.setId(rs.getString(1));
        content.setAssociatedImgs(rs.getString(2));
        content.setQuestion(rs.getString(3));

        // Create Map of answers
        String answers = rs.getString(4);
        Map<Integer,String> mapOfAnswers = new HashMap<>();
        String[] arrayOfAnswers = null;

        if(answers!=null && !answers.isEmpty()) {

            List<String> listOfAnswers = Arrays.asList(answers.split("\\|"));

                    /*
                    for (String answer: listOfAnswers) {
                        String[] answersArray = answer.split(":");
                        mapOfAnswers.put(Integer.valueOf(answersArray[0]),answersArray[1]);
                    }*/
            arrayOfAnswers = listOfAnswers.toArray(new String[listOfAnswers.size()]);
        }
        content.setAnswer(arrayOfAnswers);

        String rightAnswers = rs.getString(5);
        String[] arrayOfRightAnswers = null;
        if(rightAnswers!=null && !rightAnswers.isEmpty()) {

            List<String> listOfRightAnswers = Arrays.asList(rightAnswers.split("\\|"));

                    /*
                    for (String answer: listOfAnswers) {
                        String[] answersArray = answer.split(":");
                        mapOfAnswers.put(Integer.valueOf(answersArray[0]),answersArray[1]);
                    }*/
            arrayOfRightAnswers = listOfRightAnswers.toArray(new String[listOfRightAnswers.size()]);
        }

        content.setRightAnswer(arrayOfRightAnswers);

        content.setWeight(rs.getInt(6));
        content.setCrTimestamp(rs.getTimestamp(7));
        content.setContentTag(rs.getString(8));
        content.setTopicTag(rs.getString(9));
        content.setCrUserEmail(rs.getString(10));
        content.setComments(rs.getString(11));
        content.setAprEmail(rs.getString(12));
        content.setAprStatus(rs.getString(13));

        return content;

    }

    public TokenValidationResult checkToken(String uuid) {
        PreparedStatement stmt;
        TokenValidationResult tokenValidationResult = new TokenValidationResult();
        try {
            String insertQuery = "select TIMESTAMPDIFF(HOUR, createdDate, CURRENT_TIMESTAMP), `email` from live_tokens where token = ?";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, uuid);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int hours = rs.getInt(1);
                tokenValidationResult.setEmail(rs.getString(2));
                if (hours > 6) {
                    tokenValidationResult.setPresent(false);
                    this.deleteTokens(uuid);
                } else {
                    tokenValidationResult.setPresent(true);
                }
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while checking token: "+uuid);
            logger.error("Failed",e);
        }
        return tokenValidationResult;
    }

    public void deleteTokens(String uuid) {
        PreparedStatement stmt;
        try {
            String insertQuery = "DELETE FROM `live_tokens` WHERE token=?";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, uuid);

            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            logger.error("Error while deleting token: "+uuid);
            logger.error("Failed",e);
            e.printStackTrace();
        }
    }

    public Content getQueByID(String contentTag, String id) {
        PreparedStatement stmt;
        Content appQuestion = new Content();
        try {
            String contentCategory = getContentTypeBasedOnContentTag(contentTag);
            String randomQues = selectStr + contentCategory + " where apr_state='A' and id=?";
            stmt = con.prepareStatement(randomQues);
            stmt.setString(1,id);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                appQuestion = fillUpContent(rs);
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while getting all content for type : " + contentTag);
            logger.error("Failed",e);
        }
        return appQuestion;
    }

    public AuthResult authorizeUser(String email, String password) {
        PreparedStatement stmt;
        ResultSet rs;
        String retrievedPwd, name;

        logger.debug("Authorizing Admin user: "+email);
        try {
            String userSearchQuery = "SELECT name, email,password from `user` where email=?";
            stmt = con.prepareStatement(userSearchQuery);
            stmt.setString(1, email);

            System.out.println(userSearchQuery);
            rs = stmt.executeQuery();

            if (rs.next()) {
                retrievedPwd = rs.getString("password");
                email = rs.getString("email");
                name = rs.getString("name");

                System.out.println(retrievedPwd);
            } else {
                rs.close();
                stmt.close();
                return new AuthResult(NEW_USER,false, null, null, null);
            }

            // TODO: Decode the password
            String encryptedIncomingPwd = cryptoUtil.encrypt(secretPwdKey, password);
            if (encryptedIncomingPwd.equalsIgnoreCase(retrievedPwd)) {
                rs.close();
                stmt.close();
                return new AuthResult(name,true, email,null, null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while authorizing App user: "+email);
            logger.error("Failed",e);
        }
        logger.debug("Success: Authorizing Admin user: "+email);
        return new AuthResult(NEW_USER,false, null,null, null);
    }


    public AuthResult authorizeAppUser(String email, String schoolId, String deviceId) {
        PreparedStatement stmt;
        ResultSet rs;
        String name, sessionID, fetchedDeviceId;

        logger.debug("Authorizing App user: "+email +" , schooldID: "+schoolId+" , devideID: "+deviceId);
        try {
            String userSearchQuery = "SELECT name, sessionid, deviceId from `appuser` where email=? and schoolid=?";
            stmt = con.prepareStatement(userSearchQuery);
            stmt.setString(1, email);
            stmt.setString(2,schoolId);

            System.out.println(userSearchQuery);
            rs = stmt.executeQuery();

            if (rs.next()) {
                name = rs.getString("name");
                sessionID = rs.getString("sessionid");
                fetchedDeviceId = rs.getString("deviceId");
                System.out.println(name);
            } else {
                rs.close();
                stmt.close();
                return new AuthResult(NEW_USER,false, null, null, null);
            }

            if(fetchedDeviceId == null || fetchedDeviceId.isEmpty()) {
                // Insert the device ID, because it was removed during logoff
                String updateAppUser = "update appuser SET deviceId= ? where email=?";
                stmt = con.prepareStatement(updateAppUser);

                stmt.setString(1,deviceId);
                stmt.setString(2,email);
                stmt.executeUpdate();

                fetchedDeviceId = deviceId;
            }

            if(deviceId.equalsIgnoreCase(fetchedDeviceId)) {
                rs.close();
                stmt.close();
                return new AuthResult(name,true, email,null, sessionID);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while authorizing App user: "+email);
            logger.error("Failed",e);
        }
        logger.debug("Success: Authorizing App user: "+email +" , schooldID: "+schoolId+" , devideID: "+deviceId);
        return new AuthResult(NEW_USER, false, null,null, null);
    }


    public AppLoginResponse createSession(String email) {

        AppLoginResponse appLoginResponse = new AppLoginResponse();
        String sessionId = UUID.randomUUID().toString();

        logger.debug("Creating session for user: "+email);

        PreparedStatement stmt;
        try {
            // Get the list of content Types
            LinkedHashMap<String, String> mapOfContents = getContentTags();

            List<AppLevel> appLevels = new ArrayList<>();
            for (String contentLevel : mapOfContents.keySet()) {
                AppLevel appLevel = new AppLevel();

                // Get random questions
                List<Content> randomQues = this.getAllContentByContentType(contentLevel, quesCount, true, true);
                appLevel.setName(contentLevel);
                appLevel.setContentList(randomQues);
                appLevel.setCurrentPage(0);
                appLevel.setTotalPage(quesCount);

                appLevels.add(appLevel);
            }

            String selectCommentQuery = "insert into session (id, email, appcontent, session_state)values (?,?,?,?)";
            stmt = con.prepareStatement(selectCommentQuery);

            appLoginResponse.setLevels(appLevels);
            appLoginResponse.setName(email);
            appLoginResponse.setStatus(true);
            appLoginResponse.setMessage("Successfully logged in!");
            appLoginResponse.setSessionId(sessionId);

            String appResponseJson = PatternUtil.getJSON(appLoginResponse);
            System.out.println(appResponseJson);

            stmt.setString(1,sessionId);
            stmt.setString(2,email);
            stmt.setString(3,appResponseJson);
            stmt.setString(4,"O");

            stmt.executeUpdate();

            // Update the appuser table with sessionID
            String updateAppUser = "update appuser SET sessionid= ? where email=?";
            stmt = con.prepareStatement(updateAppUser);

            stmt.setString(1,sessionId);
            stmt.setString(2,email);
            stmt.executeUpdate();

            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }

        logger.debug("Success: Creating session for user: "+email);

        return appLoginResponse;
    }

    public AppLoginResponse getSession(String email, String sessionId) {

        // Check if a session for this email exists and is open, if closed then return a new msg saying, session already taken.
        AppLoginResponse appLoginResponse = new AppLoginResponse();
        String appLoginResponseStr, sessionStateStr;

        logger.debug("Fetching session for: "+email+" , sessionID: "+sessionId );

        PreparedStatement stmt;
        ResultSet rs;
        try {
            String userSearchQuery = "SELECT appcontent, session_state from `session` where email= ? and id = ? ";
            stmt = con.prepareStatement(userSearchQuery);
            stmt.setString(1, email);
            stmt.setString(2, sessionId);

            System.out.println(userSearchQuery);
            rs = stmt.executeQuery();

            if (rs.next()) {
                sessionStateStr = rs.getString("session_state");
                if (sessionStateStr != null) {
                    if (sessionStateStr.equalsIgnoreCase("C")) {
                        appLoginResponse.setStatus(false);
                        appLoginResponse.setMessage(
                          "You have completed the Session! Kindly contact the Administrator to re-take the test");
                    }

                    else if(sessionStateStr.equalsIgnoreCase("O")) {
                        // Return the session
                        Gson gson = new GsonBuilder().create();

                        appLoginResponseStr = rs.getString("appcontent");
                        System.out.println(appLoginResponseStr);

                        appLoginResponse = gson.fromJson(appLoginResponseStr, AppLoginResponse.class);
                        System.out.println(appLoginResponse);

                        appLoginResponse.setStatus(true);
                    }
                }
            }

            rs.close();
            stmt.close();
            return appLoginResponse;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while authorizing App user: "+email);
            logger.error("Failed",e);
        }
        logger.debug("Success: Fetching session for: "+email+" , sessionID: "+sessionId );
        return appLoginResponse;
    }


    public void updateSession(AppQuestionAnswer appQuestionAnswer) {

        AppLoginResponse appLoginResponse = this.getSession(appQuestionAnswer.getEmail(), appQuestionAnswer.getSessionId());

        logger.debug("Updating session :"+appQuestionAnswer);

        // Check if the session is Open
        if (appLoginResponse.isStatus()) {
            for (int i = 0; i < appLoginResponse.getLevels().size(); i++) {
                // Check if the Levels match
                AppLevel appLevel = appLoginResponse.getLevels().get(i);
                if (appQuestionAnswer.getLevel().equalsIgnoreCase(appLevel.getName())) {
                    // Set the current page for the level
                    int currentLevelPage = appLoginResponse.getLevels().get(i).getCurrentPage();
                    int currentAppPage = appQuestionAnswer.getPageNo();
                    if (currentAppPage > currentLevelPage) {
                        appLoginResponse.getLevels().get(i).setCurrentPage(++currentLevelPage);
                        for (int j = 0; j < appLoginResponse.getLevels().get(i).getContentList().size(); j++) {
                            // Check if the content/Question IDs match
                            Content appContent = appLoginResponse.getLevels().get(i).getContentList().get(j);
                            if (appQuestionAnswer.getQid().equalsIgnoreCase(appContent.getId())) {
                                // Then update the user answer
                                appLoginResponse.getLevels().get(i).getContentList().get(j)
                                  .setUserAnswer(appQuestionAnswer.isUserAnswer());
                            }
                        }
                    }
                }
            }
        }

        // Persist the content back
        String appResponseJson = PatternUtil.getJSON(appLoginResponse);
        System.out.println(appResponseJson);
        PreparedStatement stmt;
        try {

            String selectCommentQuery = "update session SET appcontent= ? where id=? and email=?";
            stmt = con.prepareStatement(selectCommentQuery);
            stmt.setString(1, appResponseJson);
            stmt.setString(2,appQuestionAnswer.getSessionId());
            stmt.setString(3,appQuestionAnswer.getEmail());
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while generating content type for content tags");
            logger.error("Failed",e);
        }
        logger.debug("Success: Updating session :"+appQuestionAnswer);
    }

    public void logOffAppUser(String email) {
        PreparedStatement stmt;
        try {

            String selectCommentQuery = "update appuser SET deviceId=null where email=?";
            stmt = con.prepareStatement(selectCommentQuery);
            stmt.setString(1, email);
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while logging off user:"+email);
            logger.error("Failed",e);
        }
        logger.debug("Success: logging off user:"+email);
    }

    public boolean checkSchoolExists(String schID) {

        PreparedStatement stmt;
        String schName;
        boolean schExists = false;
        try {
            String insertQuery = "select name from schools where id = ? ";
            stmt = con.prepareStatement(insertQuery);
            stmt.setString(1, schID);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                schName = rs.getString("name");
                schExists = true;
            }
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while checking schoold: "+schID);
            logger.error("Failed",e);
        }
        return schExists;

    }


    private AppUser getUserFromEmail(String email) {
        PreparedStatement stmt;
        ResultSet rs;
        AppUser appUser = new AppUser();
        try {
            String userSearchQuery = "SELECT id, name from `appuser` where email=?";
            stmt = con.prepareStatement(userSearchQuery);
            stmt.setString(1, email);

            System.out.println(userSearchQuery);
            rs = stmt.executeQuery();

            if (rs.next()) {
                appUser.setId(rs.getString("id"));
                appUser.setName(rs.getString("name"));
                System.out.println(appUser);
            }

            rs.close();
            stmt.close();
            return appUser;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while authorizing App user: "+email);
            logger.error("Failed",e);
        }
        return appUser;
    }

    public void destroy() {
        try {
            if (con != null)
                con.close();
        } catch (Exception e) {
            logger.error("Failed",e);
            e.printStackTrace();
        }
    }

    public void keepConAlive() {
        Thread t = new Thread() {
            public void run()
            {
                try {
                    setName("DB connection keep alive Thread");
                    while (true) {
                        Statement stmt = con.createStatement();
                        String query = "SELECT count(*) from `user`";
                        logger.info("Keep Alive query running: "+query);
                        ResultSet rs = stmt.executeQuery(query);
                        Thread.sleep(7200000);
                    }
                } catch (Exception e) {
                    logger.error("Failed",e);
                }

            }
        };
        t.start();
    }

    public static void main(String[] argv) {
        MySQLAdapter mySQLAdapter = new MySQLAdapter();
        mySQLAdapter.init();

        //UserPojo userPojo = new UserPojo("Venkat", "venkat@gmail.com","venkat@123");
        UserPojo userPojo = new UserPojo("Sandeep", "sandeep@gmail.com", "sandeep@123", "NA");
        AppQuestionAnswer appQuestionAnswer = new AppQuestionAnswer();
        appQuestionAnswer.setEmail("sandy@gmail.com");
        appQuestionAnswer.setLevel("Level-2");
        appQuestionAnswer.setPageNo(2);
        appQuestionAnswer.setQid("381d4b49-8da0-4ba6-8314-ab0149e1117b");
        appQuestionAnswer.setSessionId("c7b86ea9-aa3d-40b7-94bf-ee401a47e8b3");
        appQuestionAnswer.setUserAnswer(true);

        AppRegisterRequest appRegisterRequest = new AppRegisterRequest();
        appRegisterRequest.setEmail("sandy2@gmail.com");
        appRegisterRequest.setDept("CS");
        appRegisterRequest.setDeviceId(UUID.randomUUID().toString());
        appRegisterRequest.setName("Sandeep");
        appRegisterRequest.setSchoolId("SKCH21");

        NewContent content = new NewContent();
        content.setQuestion("SampleQ");
        content.setCrUserEmail("sandy@gmail.com");
        /*
        try {
            int presentCount = mySQLAdapter.searchAndRetrieveCustomerCodeCount(codePojo);
            System.out.println("Present Count: " + presentCount);


        } catch (ValueNotFoundException e) {
            System.out.println("Value not found");
        } */

        //mySQLAdapter.registerNewUser(userPojo);
        mySQLAdapter.registerNewAppUser(appRegisterRequest);
        //mySQLAdapter.authorizeUser("Sandeep@gmail.com","/EtN9+S8vnZGhMNyzJkV4g");
        try {
            //System.out.println(mySQLAdapter.getOrder("1002", "3c832196-e122-4ea4-a150-e9d6ae801f2b").toString());
            //mySQLAdapter.getAllOrders("1002").stream().forEach(System.out::println);
            //mySQLAdapter.readAndGenerateFiles("1002", "107d6a89-5d43-4a39-90e6-1aee7a3f1c50");
            //mySQLAdapter.getContentByContentType("abc@gmail.com", "content_A").stream().forEach(System.out::println);
            //mySQLAdapter.getContentByContentTags("abc2@gmail.com","primary").stream().forEach(System.out::println);
            //mySQLAdapter.createSession("sandy@gmail.com");
            //System.out.println(mySQLAdapter.getSession("sandy@gmail.com","c7b86ea9-aa3d-40b7-94bf-ee401a47e8b3"));
            //mySQLAdapter.insertContent(content);
            //mySQLAdapter.authorizeAppUser("sandy@gmail.com","SKCH21", "c7b86ea9-aa3d-40b7-94bf-ee401a47e8b3");
            //mySQLAdapter.updateSession(appQuestionAnswer);
            //mySQLAdapter.logOffAppUser("sandy@gmail.com");
            //System.out.println(mySQLAdapter.getAllContentByContentType("Level-1",5, false, true));
            //mySQLAdapter.getContentTags();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

