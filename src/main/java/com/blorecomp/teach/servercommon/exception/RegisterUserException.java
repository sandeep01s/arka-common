package com.blorecomp.teach.servercommon.exception;

public class RegisterUserException extends Exception {

    public RegisterUserException(String message) {
        super(message);
    }
}
