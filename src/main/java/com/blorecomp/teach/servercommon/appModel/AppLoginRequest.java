package com.blorecomp.teach.servercommon.appModel;

public class AppLoginRequest {

  public AppLoginRequest() {
  }

  private String email;
  private String schoolId;
  private String deviceId;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }
}
