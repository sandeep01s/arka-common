package com.blorecomp.teach.servercommon.appModel;

import java.util.List;

public class AppLoginResponse {

    private String name;
    private List<AppLevel> levels;
    private String authToken;
    private boolean status;
    private String message;
    private String sessionId;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

  public List<AppLevel> getLevels() {
    return levels;
  }

  public void setLevels(List<AppLevel> levels) {
    this.levels = levels;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  @Override public String toString() {
    return "AppLoginResponse{" +
      "name='" + name + '\'' +
      ", levels=" + levels +
      ", authToken='" + authToken + '\'' +
      ", status=" + status +
      ", message='" + message + '\'' +
      '}';
  }
}
