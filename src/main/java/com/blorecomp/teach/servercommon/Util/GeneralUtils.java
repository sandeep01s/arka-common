package com.blorecomp.teach.servercommon.Util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GeneralUtils {

    public static String getCurrentTimestamp() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime     now = LocalDateTime.now();
        return dtf.format(now);
    }
}
