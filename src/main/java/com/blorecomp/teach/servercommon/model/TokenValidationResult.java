package com.blorecomp.teach.servercommon.model;

public class TokenValidationResult {

    private boolean isPresent;
    private String email;

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
