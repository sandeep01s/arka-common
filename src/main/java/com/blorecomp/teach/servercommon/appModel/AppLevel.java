package com.blorecomp.teach.servercommon.appModel;

import com.blorecomp.teach.servercommon.model.Content;

import java.util.List;

public class AppLevel {

  private String name;
  private List<Content> contentList;
  private int currentPage;
  private int totalPage;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(int currentPage) {
    this.currentPage = currentPage;
  }

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public List<Content> getContentList() {
    return contentList;
  }

  public void setContentList(List<Content> contentList) {
    this.contentList = contentList;
  }

}
