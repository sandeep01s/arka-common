package com.blorecomp.teach.servercommon.model;

import java.util.Map;

public class NewContentPageResponse {

    Map<Integer, String> htmlTags;
    Map<String, String> contentTags;
    Map<String, String> topicTag;

    public NewContentPageResponse() {
    }

    public NewContentPageResponse(Map<Integer, String> htmlTags, Map<String, String> contentTags, Map<String, String> topicTag) {
        this.htmlTags = htmlTags;
        this.contentTags = contentTags;
        this.topicTag = topicTag;
    }

    public Map<Integer, String> getHtmlTags() {
        return htmlTags;
    }

    public void setHtmlTags(Map<Integer, String> htmlTags) {
        this.htmlTags = htmlTags;
    }

    public Map<String, String> getContentTags() {
        return contentTags;
    }

    public void setContentTags(Map<String, String> contentTags) {
        this.contentTags = contentTags;
    }

    public Map<String, String> getTopicTag() {
        return topicTag;
    }

    public void setTopicTag(Map<String, String> topicTag) {
        this.topicTag = topicTag;
    }
}
