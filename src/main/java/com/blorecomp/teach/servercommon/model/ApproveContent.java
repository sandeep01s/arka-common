package com.blorecomp.teach.servercommon.model;

public class ApproveContent {

    private String contentLevel;
    private String contentId;
    private String aprEmail;

    public ApproveContent() {
    }

    public ApproveContent(String contentLevel, String contentId, String aprEmail) {
        this.contentLevel = contentLevel;
        this.contentId = contentId;
        this.aprEmail = aprEmail;
    }

    public String getContentLevel() {
        return contentLevel;
    }

    public void setContentLevel(String contentLevel) {
        this.contentLevel = contentLevel;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getAprEmail() {
        return aprEmail;
    }

    public void setAprEmail(String aprEmail) {
        this.aprEmail = aprEmail;
    }

    @Override public String toString() {
        return "ApproveContent{" +
            "contentLevel='" + contentLevel + '\'' +
            ", contentId='" + contentId + '\'' +
            ", aprEmail='" + aprEmail + '\'' +
            '}';
    }
}
