package com.blorecomp.teach.servercommon.pojo;

public class UserPojo {

    private String id;
    private String name;
    private String email;
    private String password;

    public UserPojo() {
    }

    public UserPojo(String name, String email, String password, String clientID) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passowrd) {
        this.password = passowrd;
    }

    @Override public String toString() {
        return "UserPojo{" +
          "id='" + id + '\'' +
          ", name='" + name + '\'' +
          ", email='" + email + '\'' +
          ", password='" + password + '\'' +
          '}';
    }
}
