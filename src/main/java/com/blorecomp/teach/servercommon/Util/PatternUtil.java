package com.blorecomp.teach.servercommon.Util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtil {

    private static Logger logger = LogManager.getLogger(PatternUtil.class);

    public static boolean checkUserId(String userId) {
        // User ID should be of 4 numeric digits
        if (userId.length() == 4) {
            int numericUserId = Integer.parseInt(userId);
            if (numericUserId > 1000 && numericUserId < 9999)
                return true;
        }
        return false;
    }

    public static boolean isUserEmailValid(String email) {

        String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(email);

        boolean valid = matcher.matches();
        logger.info(email + " : " + valid);
        return valid;
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    public static void main(String[] argv) {
        isUserEmailValid("ss@gmail.com");
    }

    public static String getJSON(Object pojo) {
        Gson gson = new GsonBuilder().create();
        String jsonStr = gson.toJson(pojo);
        System.out.println(jsonStr);
        return jsonStr;
    }
}
