package com.blorecomp.teach.servercommon.appModel;

import java.util.List;
import java.util.Map;

public class AppQuestionAnswer {

    private String email;
    private String sessionId;
    private String level;
    private String qid;
    private int pageNo;
    private boolean userAnswer;

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(boolean userAnswer) {
        this.userAnswer = userAnswer;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override public String toString() {
        return "AppQuestionAnswer{" +
          "email='" + email + '\'' +
          ", sessionId='" + sessionId + '\'' +
          ", level='" + level + '\'' +
          ", qid='" + qid + '\'' +
          ", pageNo=" + pageNo +
          ", userAnswer=" + userAnswer +
          '}';
    }
}
