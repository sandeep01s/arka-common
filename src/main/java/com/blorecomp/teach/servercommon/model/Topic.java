package com.blorecomp.teach.servercommon.model;

import java.util.Date;

public class Topic {

  private String id;
  private String name;
  private String desc;
  private Date crTimestamp;

  public Topic() {
  }

  public Topic(String id, String name, String desc, Date crTimestamp) {
    this.id = id;
    this.name = name;
    this.desc = desc;
    this.crTimestamp = crTimestamp;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Date getCrTimestamp() {
    return crTimestamp;
  }

  public void setCrTimestamp(Date crTimestamp) {
    this.crTimestamp = crTimestamp;
  }
}
