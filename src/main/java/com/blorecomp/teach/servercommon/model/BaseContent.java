package com.blorecomp.teach.servercommon.model;

import java.util.Date;
import java.util.UUID;

public class BaseContent {

    private String id;
    private String htmlTypeId;
    private String associatedImgs;
    private String question;
    private int weight;
    private Date crTimestamp;
    private String crUserEmail;
    private String contentTag;
    private String topicTag;
    private String aprEmail;
    private String aprStatus;
    private String comments;


    public BaseContent() {
    }

    public BaseContent(String htmlTypeId, String associatedImgs, String question, int weight, Date crTimestamp, String crUserEmail, String contentTag,
        String topicTag, String aprEmail, String aprStatus, String comments) {
        this.htmlTypeId = htmlTypeId;
        this.associatedImgs = associatedImgs;
        this.question = question;
        this.weight = weight;
        this.crTimestamp = crTimestamp;
        this.crUserEmail = crUserEmail;
        this.contentTag = contentTag;
        this.topicTag = topicTag;
        this.aprEmail = aprEmail;
        this.aprStatus = aprStatus;
        this.comments = comments;
    }

    public void createContentID() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String uuid) {
        this.id = uuid;
    }

    public String getHtmlTypeId() {
        return htmlTypeId;
    }

    public void setHtmlTypeId(String htmlTypeId) {
        this.htmlTypeId = htmlTypeId;
    }

    public String getAssociatedImgs() {
        return associatedImgs;
    }

    public void setAssociatedImgs(String associatedImgs) {
        this.associatedImgs = associatedImgs;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Date getCrTimestamp() {
        return crTimestamp;
    }

    public void setCrTimestamp(Date crTimestamp) {
        this.crTimestamp = crTimestamp;
    }

    public String getCrUserEmail() {
        return crUserEmail;
    }

    public void setCrUserEmail(String crUserEmail) {
        this.crUserEmail = crUserEmail;
    }

    public String getContentTag() {
        return contentTag;
    }

    public void setContentTag(String contentTag) {
        this.contentTag = contentTag;
    }

    public String getTopicTag() {
        return topicTag;
    }

    public void setTopicTag(String topicTag) {
        this.topicTag = topicTag;
    }

    public String getAprEmail() {
        return aprEmail;
    }

    public void setAprEmail(String aprEmail) {
        this.aprEmail = aprEmail;
    }

    public String getAprStatus() {
        return aprStatus;
    }

    public void setAprStatus(String aprStatus) {
        this.aprStatus = aprStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override public String toString() {
        return "BaseContent{" +
            "id='" + id + '\'' +
            ", htmlTypeId='" + htmlTypeId + '\'' +
            ", associatedImgs='" + associatedImgs + '\'' +
            ", question='" + question + '\'' +
            ", weight=" + weight +
            ", crTimestamp=" + crTimestamp +
            ", crUserEmail='" + crUserEmail + '\'' +
            ", contentTag='" + contentTag + '\'' +
            ", topicTag='" + topicTag + '\'' +
            ", aprEmail='" + aprEmail + '\'' +
            ", aprStatus='" + aprStatus + '\'' +
            ", comments='" + comments + '\'' +
            '}';
    }
}
